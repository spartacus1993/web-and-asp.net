﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class ParkRangerController : Controller
    {
        private IParkRangerRepository parkRangerRepo;
        public int PageSize = 5;

        public ParkRangerController(IParkRangerRepository parkRangerRepo)
        {
            this.parkRangerRepo = parkRangerRepo;
        }

        public ViewResult List(string state, int pageId = 1)
            => View(new ParkRangerListViewModel
            {
                ParkRangers = parkRangerRepo.ParkRangers
                    .Where(pr => state == null || pr.State == state)
                    .OrderBy(pr => pr.ParkRangerId)
                    .Skip((pageId - 1) * PageSize)
                    .Take(PageSize),

                PagingInfo = new PagingInfo()
                {
                    CurrentPage = pageId, 
                    ItemsPerPage = PageSize,
                    TotalItems = state == null ?
                        parkRangerRepo.ParkRangers.Count() :
                        parkRangerRepo.ParkRangers
                        .Where(e => e.State == state)
                        .Count()
                },

                CurrentState = state


            });
        
            
        
    }
}
