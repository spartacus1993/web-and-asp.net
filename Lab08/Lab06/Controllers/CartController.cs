﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab06.Infrustructure;
using Lab06.Models;
using Lab06.Models.ViewModels;


namespace SportsStore.Controllers
{
    public class CartController : Controller
    {

        private IParkRangerRepository repository;

        public CartController(IParkRangerRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index(string returnURL)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnURL = returnURL
            });
        }

        public RedirectToActionResult AddToCart(int parkRangerId, string returnURL)
        {
            ParkRanger parkRanger = repository.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (parkRanger != null)
            {
                Cart cart = GetCart();
                cart.AddItem(parkRanger, 1);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromCart(int parkRangerId, string returnURL)
        {
            ParkRanger parkRanger = repository.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (parkRanger != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(parkRanger);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });

        }

        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }



    }
}