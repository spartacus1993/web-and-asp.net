﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models.ViewModels
{
    public class ParkRangerListViewModel
    {
        public IEnumerable<ParkRanger> ParkRangers { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentState { get; set; }
    }
}
