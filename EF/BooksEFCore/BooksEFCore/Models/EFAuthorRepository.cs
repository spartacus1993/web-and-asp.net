﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class EFAuthorRepository : IAuthorRepository
    {

        public IEnumerable<Author> Authors => context.Authors
            .Include(a => a.TitleAuthors)
            .ThenInclude(ta => ta.Title)
            .ThenInclude(t => t.Publisher);

        private BookDbContext context;

        public EFAuthorRepository(BookDbContext context)
        {
            this.context = context;
        }
    }
}
