﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CDPortfolio.Models
{
    public class CDResponse
    {
        [Required(ErrorMessage = "Please select a Bank")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "Please select a Term Rate")]
        public string Term { get; set; }

        [Required(ErrorMessage = "Please enter a Rate")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid Rate")]
        public string Rate { get; set; }

        [Required(ErrorMessage = "Please enter a Deposit Amount")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid Deposit")]
        public string DepositAmount { get; set; }

        [Required(ErrorMessage = "Please enter a Date")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date Format")]
        public string PurchaseDate { get; set; }

        public string MaturityDate
        {
            get
            {
                DateTime convertPurchaseDate = Convert.ToDateTime(PurchaseDate);
                int convertTerm = Convert.ToInt32(Term);
                DateTime addYears = convertPurchaseDate.AddYears(convertTerm / 12);
                addYears = addYears.Date;
                string convertAddYears = Convert.ToString(addYears);
                return convertAddYears;
            }

        }

        public double MaturityValue
        {
            get
            {
                int convertDepositAmount = Convert.ToInt32(DepositAmount);
                int convertRate = Convert.ToInt32(Rate);
                int convertTerm = Convert.ToInt32(Term);
                double r = (convertRate * 0.01 / 12);
                double power = (Math.Pow(1 + r, convertTerm));
                double total = convertDepositAmount * power;
                return Math.Round(total, 2);
            }
        }

    }
}
