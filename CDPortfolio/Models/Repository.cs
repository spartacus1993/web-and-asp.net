﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDPortfolio.Models
{
    public static class Repository
    {
        private static List<CDResponse> responses = new List<CDResponse>();

        public static IEnumerable<CDResponse> Responses
        {
            get
            {
                return responses;
            }
        }

        public static void AddResponse(CDResponse response)
        {
            responses.Add(response);
        }


    }
}
