﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CDPortfolio.Models;

namespace CDPortfolio.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(CDResponse cdResponse)
        {
            
            IEnumerable<CDResponse> repo = Repository.Responses;
            
            if(repo != null && repo.Any())
            {
                return View(repo);
            }
            else
            {
                return View();
            }
           
        }

        [HttpGet]
        public ViewResult CDForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult CDForm(CDResponse cdResponse)
        {
            if (ModelState.IsValid)
            {
                Repository.AddResponse(cdResponse);
                IEnumerable<CDResponse> repo = Repository.Responses;
                return View("Index", repo);
            }
            else
            {
                return View();
            }
            
        }
        
    }
}
