﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class ParkRanger
    {
        public int ParkRangerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string State { get; set; }
        public bool? BachelorDegree { get; set; }

        // Associations
        public List<StatePark> StateParks { get; set; }


    }
}
