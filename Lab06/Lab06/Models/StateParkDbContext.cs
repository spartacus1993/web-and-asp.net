﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Lab06.Models
{
    public class StateParkDbContext : DbContext
    {
        public StateParkDbContext(DbContextOptions<StateParkDbContext> options) : base(options)
        {

        }

        public DbSet<StatePark> StateParks { get; set; }
        public DbSet<ParkRanger> ParkRangers { get; set; }
        public DbSet<ParkActivity> ParkActivities { get; set; }
        public DbSet<StateParkActivity> StateParkActivities { get; set; }


    }
}
