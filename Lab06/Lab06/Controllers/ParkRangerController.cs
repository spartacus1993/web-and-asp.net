﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class ParkRangerController : Controller
    {
        private IParkRangerRepository parkRangerRepo;
        private int PageSize = 5;

        public ParkRangerController(IParkRangerRepository parkRangerRepo)
        {
            this.parkRangerRepo = parkRangerRepo;
        }

        public IActionResult List(int page = 1)
        {
            IEnumerable<ParkRanger> parkRangers = parkRangerRepo.ParkRangers
                .OrderBy(pr => pr.ParkRangerId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize);

            PagingInfo pagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = parkRangerRepo.ParkRangers.Count()
            };

            ParkRangerListViewModel vModel = new ParkRangerListViewModel()
            {
                ParkRangers = parkRangers,
                PagingInfo = pagingInfo
            };

            return View(vModel);
        }
    }
}
