﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab06.Models;
using Lab06.Models.ViewModels;


namespace Lab06.Controllers
{
    public class StateParkController : Controller
    {
        private IStateParkRepository stateParkRepo;
        private int PageSize = 5;

        public StateParkController(IStateParkRepository stateParkRepo)
        {
            this.stateParkRepo = stateParkRepo;
        }

        public IActionResult List(int page = 1)
        {

            IEnumerable<StatePark> stateParks = stateParkRepo.StateParks
                .OrderBy(sp => sp.StateParkId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize);

            PagingInfo pagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = stateParkRepo.StateParks.Count()
       
            };

            StateParksListViewModel vModel = new StateParksListViewModel()
            {
                StateParks = stateParks,
                PagingInfo = pagingInfo
            };

            return View(vModel);
        }

    }
}
