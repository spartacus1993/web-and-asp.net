﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Lab06.Models;


namespace Lab06
{
    public class Startup
    {

        IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsetting.json").Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            

            // string conn = @"Data Source=colonriverawestern.database.windows.net;Initial Catalog=colonrivera;User ID=colonrivera;Password=Jman1993;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            // services.AddDbContext<StateParkDbContext>(options =>
            //                 options.UseSqlServer(conn)
            // );

            services.AddDbContext<StateParkDbContext>(
                options => options.UseSqlServer(
                    Configuration["Data:Lab06:ConnectionString"]
                    ));

            services.AddTransient<IStateParkRepository, EFStateParkRepository>();
            services.AddTransient<IParkRangerRepository, EFParkRangerRepository>();
            services.AddMvc();
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, StateParkDbContext context)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            // app.UseMvcWithDefaultRoute();

            
            app.UseMvc(
                routes =>
                {
                routes.MapRoute(
                    name: "pagination",
                    template: "Products/Page{page}",
                    defaults: new { Controller = "Product", action = "List" }
                    );

                    routes.MapRoute(
                        name: "default",
                        template: "{controller=StatePark}/{action=List}/{id?}");

                });
                

           // SeedData.EnsurePopulated(context);

        }
    }
}
