﻿using Lab06.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class Profile
    {
        private List<ProfileLine> lineCollection = new List<ProfileLine>();   

        public virtual void ViewParkRanger(ParkRanger parkRanger, int quantity)
        {
            ProfileLine line = lineCollection
                .Where(pr => pr.ParkRanger.ParkRangerId == parkRanger.ParkRangerId)
                .FirstOrDefault();

            if (line == null)
            {

               Clear();
                
                lineCollection.Add(new ProfileLine
                {
                    ParkRanger = parkRanger,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
                
            }

        }

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<ProfileLine> Lines => lineCollection;
    }

    public class ProfileLine
    {
        public int ProfileLineId { get; set; }
        public ParkRanger ParkRanger { get; set; }
        public int Quantity { get; set; }

    }

}
