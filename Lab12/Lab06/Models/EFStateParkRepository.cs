﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFStateParkRepository : IStateParkRepository
    {
        public IEnumerable<StatePark> StateParks => context.StateParks.Include(sp => sp.StateParkActivity).ThenInclude(spa => spa.ParkActivity);

        private StateParkDbContext context;

        public EFStateParkRepository(StateParkDbContext context)
        {
            this.context = context;
        }

    }
}
