﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class StateParkActivity
    {
        public int StateParkActivityId { get; set; }

        // Associations
        public StatePark StatePark { get; set; }
        public int StateParkId { get; set; }

        public ParkActivity ParkActivity { get; set; }
        public int ParkActivityId { get; set; }


    }
}
