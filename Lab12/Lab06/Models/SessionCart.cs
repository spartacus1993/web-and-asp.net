﻿using Lab06.Infrustructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class SessionCart : Cart
    {

        [JsonIgnore]
        public ISession Session { get; set; }

        public static Cart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            SessionCart cart = session?.GetJson<SessionCart>("Cart") ??
                new SessionCart();

            cart.Session = session;
            return cart;

        }

        public override void AddItem(ParkRanger parkRanger, int quantity)
        {
            base.AddItem(parkRanger, quantity);
            Session.SetJson("Cart", this);
        }

        public override void RemoveLine(ParkRanger parkRanger)
        {
            base.RemoveLine(parkRanger);
            Session.SetJson("Cart", this);
        }

        public override void Clear()
        {
            base.Clear();
            Session.SetJson("Cart", this);
        }
    }
}
