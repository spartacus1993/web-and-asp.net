﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class ParkRanger
    {
        public int ParkRangerId { get; set; }

        [Required(ErrorMessage = "FIRST NAME IS REQUIRED")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LAST NAME IS REQUIRED")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "STATE IS REQUIRED")]
        public string State { get; set; }

        [Required(ErrorMessage = "BACHELOR DEGREE IS REQUIRED")]
        public bool? BachelorDegree { get; set; }

        [Required(ErrorMessage = "BIO IS REQUIRED")]
        public string Bio { get; set; }

        [Required(ErrorMessage = "SUNDAY SCHEDULE IS REQUIRED")]
        public string ScheduleSun { get; set; }

        [Required(ErrorMessage = "MONDAY SCHEDULE IS REQUIRED")]
        public string ScheduleMon { get; set; }

        [Required(ErrorMessage = "TUESDAY SCHEDULE IS REQUIRED")]
        public string ScheduleTue { get; set; }

        [Required(ErrorMessage = "WEDNESDAY SCHEDULE IS REQUIRED")]
        public string ScheduleWed { get; set; }

        [Required(ErrorMessage = "THURSDAY SCHEDULE IS REQUIRED")]
        public string ScheduleThur { get; set; }

        [Required(ErrorMessage = "FRIDAY SCHEDULE IS REQUIRED")]
        public string ScheduleFri { get; set; }

        [Required(ErrorMessage = "SATURDAY SCHEDULE IS REQUIRED")]
        public string ScheduleSat { get; set; }


        // Associations
        public List<StatePark> StateParks { get; set; }


    }
}
