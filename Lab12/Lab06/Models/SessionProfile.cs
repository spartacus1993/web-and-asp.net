﻿using Lab06.Infrustructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class SessionProfile : Profile
    {

        [JsonIgnore]
        public ISession Session { get; set; }

        public static Profile GetProfile(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            SessionProfile profile = session?.GetJson<SessionProfile>("Profile") ??
                new SessionProfile();

            profile.Session = session;
            return profile;

        }

        public override void ViewParkRanger(ParkRanger parkRanger, int quantity)
        {
            base.ViewParkRanger(parkRanger, quantity);
            Session.SetJson("Profile", this);
        }

    }
}

