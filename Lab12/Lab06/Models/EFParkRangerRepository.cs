﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFParkRangerRepository : IParkRangerRepository
    {
        private StateParkDbContext context;
        public IEnumerable<ParkRanger> ParkRangers => context.ParkRangers;

        public EFParkRangerRepository(StateParkDbContext context)
        {
            this.context = context;
        }

        public void Save(ParkRanger parkRanger)
        {
            if(parkRanger.ParkRangerId == 0)
            {
                // add new
                context.ParkRangers.Add(parkRanger);
            }
            else
            {
                // update
                ParkRanger dbEntry = context.ParkRangers
                    .FirstOrDefault(pr => pr.ParkRangerId == parkRanger.ParkRangerId);
                if(dbEntry != null)
                {
                    dbEntry.FirstName = parkRanger.FirstName;
                    dbEntry.LastName = parkRanger.LastName;
                    dbEntry.State = parkRanger.State;
                    dbEntry.BachelorDegree = parkRanger.BachelorDegree;
                    dbEntry.Bio = parkRanger.Bio;
                    dbEntry.ScheduleSun = parkRanger.ScheduleSun;
                    dbEntry.ScheduleMon = parkRanger.ScheduleMon;
                    dbEntry.ScheduleTue = parkRanger.ScheduleTue;
                    dbEntry.ScheduleWed = parkRanger.ScheduleWed;
                    dbEntry.ScheduleThur = parkRanger.ScheduleThur;
                    dbEntry.ScheduleFri = parkRanger.ScheduleFri;
                    dbEntry.ScheduleSat = parkRanger.ScheduleSat;

                }
            }

            // save
            context.SaveChanges();
        }

        public ParkRanger Delete(int parkRangerId)
        {
            ParkRanger dbEntry = context.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (dbEntry != null)
            {
                context.ParkRangers.Remove(dbEntry);
                context.SaveChanges();
            }

            return dbEntry;
        }
    }
}
