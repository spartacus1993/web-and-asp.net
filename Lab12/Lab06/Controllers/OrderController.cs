﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class OrderController : Controller
    {
        private IOrderRepository orderRepo;
        private Cart cart;

        public OrderController(IOrderRepository orderRepo, Cart cart)
        {
            this.orderRepo = orderRepo;
            this.cart = cart;
        }

        [Authorize]
        public ViewResult List() =>
            View(orderRepo.Orders.Where(o => !o.Contacted));

        [HttpPost]
        [Authorize]
        public IActionResult MarkContacted(int orderId)
        {
            Order order = orderRepo.Orders
                .FirstOrDefault(o => o.OrderId == orderId);

            if(order != null)
            {
                order.Contacted = true;
                orderRepo.SaveOrder(order);
            }

            return RedirectToAction(nameof(List));


        }


        public IActionResult Checkout() => View(new Order());


        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if(cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry your cart is empty");
            }

            if (ModelState.IsValid)
            {
                order.Lines = cart.Lines.ToArray();
                orderRepo.SaveOrder(order);
                return RedirectToAction(nameof(Completed));
            }

            return View(order);

        }

        public ViewResult Completed()
        {
            cart.Clear();
            return View();
        }

    }

}