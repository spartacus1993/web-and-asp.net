﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab06.Infrustructure;
using Lab06.Models;
using Lab06.Models.ViewModels;


namespace SportsStore.Controllers
{
    public class CartController : Controller
    {

        private IParkRangerRepository repo;
        private Cart cart;

        public CartController(IParkRangerRepository repo, Cart cart)
        {
            this.repo = repo;
            this.cart = cart;
        }

        public ViewResult Index(string returnURL)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnURL = returnURL
            });
        }

        public RedirectToActionResult AddToCart(int parkRangerId, string returnURL)
        {
            ParkRanger parkRanger = repo.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (parkRanger != null)
            {
                cart.AddItem(parkRanger, 1);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromCart(int parkRangerId, string returnURL)
        {
            ParkRanger parkRanger = repo.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (parkRanger != null)
            {
                cart.RemoveLine(parkRanger);
            }

            return RedirectToAction("Index", new { returnURL });

        }
    }
}