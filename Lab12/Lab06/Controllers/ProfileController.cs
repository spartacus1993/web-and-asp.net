﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class ProfileController : Controller
    {
        private IParkRangerRepository repo;
        private Profile profile;

        public ProfileController(IParkRangerRepository repo, Profile profile)
        {
            this.repo = repo;
            this.profile = profile;
            
        }

        public ViewResult Index(string returnURL)
        {
            return View(new ProfileIndexViewModel
            {
                Profile = profile,
                ReturnURL = returnURL
            });
        }

        public RedirectToActionResult ViewProfile(int parkRangerId, string returnURL)
        {
            ParkRanger parkRanger = repo.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId);

            if (parkRanger != null)
            {
                profile.ViewParkRanger(parkRanger, 1);
            }

            return RedirectToAction("Index", new { returnURL });
        }

    }
}