﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LanguageFeatures.Models;

namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {

        public ViewResult Index()
        {
            var products = new[]
            {
                new { Name = "Kayak", Price = 275M },
                new { Name = "Lifejacket", Price = 48.5M },
                new { Name = "Soccer ball", Price = 19.50M },
                new { Name = "Corner flag", Price = 34.95M }
            };

            return View(products.Select(p => $"{nameof(p.Name)}: {p.Name}, {nameof(p.Price)}: {p.Price}"));

            //return View(products.Select(p => $"Name: {p.Name}, Price: {p.Price}"));


            //public async Task<ViewResult> Index()
            //{
            //    long? length = await MyAsyncMethods.GetPageLength();
            //    return View(new string[] { $"Length: {length}" });
            //}

            //public ViewResult Index()
            //{
            //    var products = new[]
            //    {
            //        new { Name = "Kayak", Price = 275M },
            //        new { Name = "Lifejacket", Price = 48.5M },
            //        new { Name = "Soccer ball", Price = 19.50M },
            //        new { Name = "Corner flag", Price = 34.95M }
            //    };


            //    return View(products.Select(p => p?.GetType().Name));
            //    // return View(products.Select(p => p?.Name));


            //}



            //public ViewResult Index()
            //{
            //    var names = new[]
            //    {
            //        "Kayak", "Lifejacket", "Soccer ball"
            //    };


            //    return View(names);

            //}



            //public ViewResult Index() => View(Product.GetProducts().Select(p => p?.Name));

            //public ViewResult Index()
            //{
            //    // Get a collection of Products
            //    // and use a lambda expression to return a collection of just names
            //    return View(Product.GetProducts().Select(p => p?.Name));
            //}

            // Defining the delegate function for
            // filtering by price...
            //bool FilterByPrice(Product p)
            //{
            //    return (p?.Price ?? 0) >= 20;
            //}

            //public ViewResult Index()
            //{
            //    ShoppingCart cart = new ShoppingCart
            //    {
            //        Products = Product.GetProducts()
            //    };

            //    Product[] productArray =
            //    {
            //        new Product {Name = "Kayak", Price = 275M },
            //        new Product{Name = "Lifejacket", Price = 48.95M },
            //        new Product{Name = "Soccer ball", Price = 19.50M },
            //        new Product{Name = "Corner flag", Price = 34.95M }
            //    };

            //    // Defining our delegate function for filtering names...
            //    Func<Product, bool> nameFilter = delegate (Product prod)
            //    {
            //        return prod?.Name?[0] == 'S';
            //    };

            //    decimal priceFilterTotal = productArray.Filter(p => (p?.Price ?? 0) >= 20).TotalPrices();
            //    decimal nameFilterTotal = productArray.Filter(p => p?.Name?[0] == 'S').TotalPrices();

            //    //decimal priceFilterTotal = productArray.Filter(FilterByPrice).TotalPrices();
            //    //decimal nameFilterTotal = productArray.Filter(nameFilter).TotalPrices();

            //    //decimal priceFilterTotal = productArray.FilterByPrice(20).TotalPrices();
            //    //decimal nameFilterTotal = productArray.FilterByName('S').TotalPrices();

            //    return View("Index", new string[]
            //    {
            //        $"Price Total: {priceFilterTotal:C2}",
            //        $"Name Total: {nameFilterTotal:C2}"
            //    });

            //    //decimal arrayTotal = productArray.FilterByPrice(20).TotalPrices();

            //    //return View("Index", new string[] { $"Array Total: {arrayTotal:C2}" });

            //    // decimal cartTotal = cart.TotalPrices();
            //    // decimal arrayTotal = productArray.TotalPrices();

            //    //return View("Index", new string[] 
            //    //{
            //    //    $"Total: {cartTotal:C2}",
            //    //    $"Array Total: {arrayTotal:C2}"
            //    //});


            //}


            //public ViewResult Index()
            //{
            //    Dictionary<string, Product> products = new Dictionary<string, Product>
            //    {
            //        ["Kayak"] = new Product { Name = "Kayak", Price = 275M },
            //        ["Lifejacket"] = new Product { Name = "Lifejacket", Price = 48.95M }
            //    };

            //    return View("Index", products.Keys);

            //}

            //public ViewResult Index()
            //{
            //    Dictionary<string, Product> products = new Dictionary<string, Product>
            //    {
            //        {"Kayak", new Product{Name = "Kayak", Price = 275M} },
            //        {"Lifejacket", new Product{Name = "Lifejacket", Price = 48.95M} }
            //    };

            //    return View("Index", products.Keys);

            //}

            //public ViewResult Index()
            //{
            //    List<string> results = new List<string>();

            //    foreach(Product p in Product.GetProducts())
            //    {
            //        string name = p?.Name ?? "<No Name>";
            //        decimal? price = p?.Price ?? 0;
            //        string relatedName = p?.Related?.Name ?? "<None>";

            //        results.Add(string.Format($"Name: {name}, Price: {price}, Related: {relatedName}"));
            //    }

            //    return View(results);

            //}

            // Hard Way
            //public ViewResult Index()
            //{
            //    string[] names = new string[3];
            //    names[0] = "Greg";
            //    names[1] = "Sophie";
            //    names[2] = "Bryden";
            //}

            // Using Collection Initializers
            //public ViewResult Index()
            //{
            //    string[] namesCI = new string[]
            //    {
            //        "Greg", "Sophie", "Bryden";
            //    }
            //}




        }

    }

    //public class HomeController : Controller
    //{
    //    public ViewResult Index()
    //    {
    //        return View(new string[] { "C#", "Language", "Features" });
    //    }
    //}

}
