﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;

namespace SportsStore.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IProductRepository prodRepo;

        public AdminController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public IActionResult Index() => View(prodRepo.Products);

        public ViewResult Edit(int productId) =>
            View(prodRepo.Products
                .FirstOrDefault(p => p.ProductId == productId));

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            
            if (!ModelState.IsValid)
            {
                // There is something wrong with the data values
                return View(product);
            }
            else
            {
                prodRepo.Save(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            

            // return View(prodRepo.Products.FirstOrDefault(p => p.ProductId == productId));
        }

        public IActionResult Create()
        {
            return View("Edit", new Product());
        }

        public IActionResult Delete(int productId)
        {
            Product p = prodRepo.Delete(productId);
            TempData["message"] = $"{p.Name} has been deleted";
            return RedirectToAction("Index");
        }


    }
}