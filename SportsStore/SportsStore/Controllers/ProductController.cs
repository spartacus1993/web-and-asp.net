﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        public int PageSize = 4;

        public ProductController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public ViewResult List(string category, int pageId = 1)
            => View(new ProductsListViewModel
            {
                Products = prodRepo.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductId)
                .Skip((pageId - 1) * PageSize)
                .Take(PageSize),

                PagingInfo = new PagingInfo()
                {
                    CurrentPage = pageId,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ?
                        prodRepo.Products.Count() :
                        prodRepo.Products
                        .Where(e => e.Category == category)
                        .Count()
                },

                CurrentCategory = category
            });

    }
}