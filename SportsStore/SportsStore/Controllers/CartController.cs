﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Infrastructure;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {

        private IProductRepository prodRepo;
        private Cart cart;

        public CartController(IProductRepository prodRepo, Cart cart)
        {
            this.prodRepo = prodRepo;
            this.cart = cart;
        }

        public ViewResult Index(string returnURL)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnURL = returnURL
            });
        }

        public RedirectToActionResult AddToCart(int productId, string returnURL)
        {
            Product product = prodRepo.Products
                .FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                cart.AddItem(product, 1);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnURL)
        {
            Product product = prodRepo.Products
                .FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                
                cart.RemoveLine(product);
                
            }

            return RedirectToAction("Index", new { returnURL });

        }



    }
}