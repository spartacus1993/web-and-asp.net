﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class EFProductRepository : IProductRepository
    {

        private ApplicationDbContext context;

        public EFProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Product> Products => context.Products;

        public void Save(Product product)
        {
            if (product.ProductId == 0)
            {
                // add new
                context.Products.Add(product);
            }
            else
            {
                // update
                Product dbEntry = context.Products
                    .FirstOrDefault(p => p.ProductId == product.ProductId);
                if(dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Price = product.Price;
                    dbEntry.Category = product.Category;
                }
            }
            // save
            context.SaveChanges();
        }

        public Product Delete(int productId)
        {
            Product dbEntry = context.Products
                .FirstOrDefault(p => p.ProductId == productId);

            if (dbEntry != null)
            {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

    }
}
