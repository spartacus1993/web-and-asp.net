﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class Product
    {
        
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter a Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a Description")]
        public string Description { get; set; }

        [Required][Range(0.01, double.MaxValue, ErrorMessage ="Please enter a positive price")]
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Please specify a Category")]
        public string Category { get; set; }
    }
}
