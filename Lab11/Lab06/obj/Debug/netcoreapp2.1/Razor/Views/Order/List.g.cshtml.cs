#pragma checksum "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6849e509d54faddb8f46f3aa674fe23968e7cda5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Order_List), @"mvc.1.0.view", @"/Views/Order/List.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Order/List.cshtml", typeof(AspNetCore.Views_Order_List))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\_ViewImports.cshtml"
using Lab06.Models;

#line default
#line hidden
#line 3 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\_ViewImports.cshtml"
using Lab06.Models.ViewModels;

#line default
#line hidden
#line 5 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\_ViewImports.cshtml"
using Lab06.Infrustructure;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6849e509d54faddb8f46f3aa674fe23968e7cda5", @"/Views/Order/List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"04df2c22b2853f93d9c6258c739d8f64f671253a", @"/Views/_ViewImports.cshtml")]
    public class Views_Order_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Order>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "MarkContacted", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(27, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
  
    ViewBag.Title = "Orders";
    Layout = "_AdminLayout";

#line default
#line hidden
            BeginContext(97, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
 if(Model.Count() > 0)
{

#line default
#line hidden
            BeginContext(126, 161, true);
            WriteLiteral("    <table class=\"table table-bordered table-striped\">\r\n        <tr><th>Company Name</th><th>Zip</th><th>Email</th><th colspan=\"2\">Details</th><th></th></tr>\r\n\r\n");
            EndContext();
#line 13 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
         foreach (Order o in Model)
        {

#line default
#line hidden
            BeginContext(335, 38, true);
            WriteLiteral("            <tr>\r\n                <td>");
            EndContext();
            BeginContext(374, 13, false);
#line 16 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
               Write(o.CompanyName);

#line default
#line hidden
            EndContext();
            BeginContext(387, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(415, 5, false);
#line 17 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
               Write(o.Zip);

#line default
#line hidden
            EndContext();
            BeginContext(420, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(448, 7, false);
#line 18 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
               Write(o.Email);

#line default
#line hidden
            EndContext();
            BeginContext(455, 121, true);
            WriteLiteral("</td>\r\n                <th>New Hire</th>\r\n                <th>Quantity</th>\r\n\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(576, 309, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2e4e4537dcee4fdb916da6113c41919a", async() => {
                BeginContext(623, 61, true);
                WriteLiteral("\r\n                        <input type=\"hidden\" name=\"orderId\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 684, "\"", 702, 1);
#line 24 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
WriteAttributeValue("", 692, o.OrderId, 692, 10, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(703, 175, true);
                WriteLiteral(" />\r\n                        <button type=\"submit\" class=\"btn btn-sm btn-danger\">\r\n                            Contact\r\n                        </button>\r\n                    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(885, 44, true);
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n");
            EndContext();
#line 32 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
             foreach (CartLine line in o.Lines)
            {

#line default
#line hidden
            BeginContext(995, 89, true);
            WriteLiteral("                <tr>\r\n                    <td colspan=\"2\"></td>\r\n                    <td>");
            EndContext();
            BeginContext(1085, 25, false);
#line 36 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
                   Write(line.ParkRanger.FirstName);

#line default
#line hidden
            EndContext();
            BeginContext(1110, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1142, 24, false);
#line 37 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
                   Write(line.ParkRanger.LastName);

#line default
#line hidden
            EndContext();
            BeginContext(1166, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1198, 13, false);
#line 38 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
                   Write(line.Quantity);

#line default
#line hidden
            EndContext();
            BeginContext(1211, 61, true);
            WriteLiteral("</td>\r\n                    <td></td>\r\n                </tr>\r\n");
            EndContext();
#line 41 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
            }

#line default
#line hidden
#line 41 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
             

        }

#line default
#line hidden
            BeginContext(1300, 16, true);
            WriteLiteral("\r\n    </table>\r\n");
            EndContext();
#line 46 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
}
else
{

#line default
#line hidden
            BeginContext(1328, 49, true);
            WriteLiteral("    <div class=\"text-center\">No New Hires</div>\r\n");
            EndContext();
#line 50 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Lab11\Lab06\Views\Order\List.cshtml"
}

#line default
#line hidden
            BeginContext(1380, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Order>> Html { get; private set; }
    }
}
#pragma warning restore 1591
