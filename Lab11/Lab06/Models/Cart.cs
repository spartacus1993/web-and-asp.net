﻿using Lab06.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual void AddItem(ParkRanger parkRanger, int quantity)
        {
            CartLine line = lineCollection
                .Where(pr => pr.ParkRanger.ParkRangerId == parkRanger.ParkRangerId)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    ParkRanger = parkRanger,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }


        public virtual void RemoveLine(ParkRanger parkRanger) =>
            lineCollection.RemoveAll(l => l.ParkRanger.ParkRangerId == parkRanger.ParkRangerId);

        public virtual decimal ComputeTotalValue() =>
            lineCollection.Sum(e => e.Quantity);

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public ParkRanger ParkRanger { get; set; }
        public int Quantity { get; set; }
    }

}
