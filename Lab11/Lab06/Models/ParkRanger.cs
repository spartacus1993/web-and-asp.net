﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class ParkRanger
    {
        public int ParkRangerId { get; set; }

        [Required(ErrorMessage = "FIRST NAME IS REQUIRED")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LAST NAME IS REQUIRED")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "STATE IS REQUIRED")]
        public string State { get; set; }

        [Required(ErrorMessage = "BACHELOR DEGREE IS REQUIRED")]
        public bool? BachelorDegree { get; set; }
 

        // Associations
        public List<StatePark> StateParks { get; set; }


    }
}
