﻿using System.Collections.Generic;
using WWVS.Controllers;
using WWVS.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace WWVS.Tests
{
    public class HomeControllerTests
    {

        class ModelCompleteFakeRepository : IRepository
        {
            public IEnumerable<Product> Products { get; } = new Product[]
            {
                //new Product{Name = "P1", Price = 275M},
                new Product{Name = "P1", Price = 5M},
                new Product{Name = "P2", Price = 48.95M},
                new Product{Name = "P3", Price = 19.50M},
                new Product{Name = "P4", Price = 35.95M}
            };

            public void AddProduct(Product p)
            {
                // do nothing - not required for test
            }

        }

        // Running the test 
        [Fact]
        public void IndexActionModelIsComplete()
        {
            // Arrange (Creating new conditions for the test)
            var controller = new HomeController();
            controller.Repository = new ModelCompleteFakeRepository();

            // Act (Performing the test)
            var model = (controller.Index() as ViewResult)?.ViewData.Model as IEnumerable<Product>;

            // Assert (verifying that the result was the one that was expected)
            Assert.Equal(controller.Repository.Products, model, Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name && p1.Price == p2.Price));

        }
    }


}

