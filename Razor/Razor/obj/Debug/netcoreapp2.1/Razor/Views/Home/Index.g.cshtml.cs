#pragma checksum "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "58ae36984accf0016aab95d64d23c876ab97ae67"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\_ViewImports.cshtml"
using Razor.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"58ae36984accf0016aab95d64d23c876ab97ae67", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"853327a25da8118a46f0da11b885907b8eb43b13", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Product>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(16, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
  
    // Layout = "_BasicLayout";
    ViewBag.Title = "Product Name";

#line default
#line hidden
            BeginContext(95, 57, true);
            WriteLiteral("\r\n<div data-productid=\"Model.ProductID\" data-stocklevel=\"");
            EndContext();
            BeginContext(153, 18, false);
#line 8 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                                                  Write(ViewBag.StockLevel);

#line default
#line hidden
            EndContext();
            BeginContext(171, 35, true);
            WriteLiteral("\">\r\n    <p>\r\n        Product Name: ");
            EndContext();
            BeginContext(207, 10, false);
#line 10 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                 Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(217, 46, true);
            WriteLiteral("\r\n    </p>\r\n\r\n    <p>\r\n        Product Price: ");
            EndContext();
            BeginContext(265, 20, false);
#line 14 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                   Write($"{ Model.Price:C2}");

#line default
#line hidden
            EndContext();
            BeginContext(286, 48, true);
            WriteLiteral("\r\n    </p>\r\n\r\n    <p>\r\n        Stock Level: \r\n\r\n");
            EndContext();
#line 20 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
         switch (ViewBag.StockLevel)
        {
            case 0:

#line default
#line hidden
            BeginContext(404, 16, true);
            WriteLiteral("                ");
            EndContext();
            BeginContext(422, 14, true);
            WriteLiteral("Out of Stock\r\n");
            EndContext();
#line 24 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                break;

            case 1:
            case 2:
            case 3:

#line default
#line hidden
            BeginContext(525, 30, true);
            WriteLiteral("                <b>Low Stock (");
            EndContext();
            BeginContext(556, 18, false);
#line 29 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                         Write(ViewBag.StockLevel);

#line default
#line hidden
            EndContext();
            BeginContext(574, 7, true);
            WriteLiteral(")</b>\r\n");
            EndContext();
#line 30 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                break;
            default:

#line default
#line hidden
            BeginContext(627, 16, true);
            WriteLiteral("                ");
            EndContext();
            BeginContext(645, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(647, 18, false);
#line 32 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
              Write(ViewBag.StockLevel);

#line default
#line hidden
            EndContext();
            BeginContext(665, 11, true);
            WriteLiteral(" in Stock\r\n");
            EndContext();
#line 33 "C:\Users\003019312\Desktop\IT-Web and Software Development\Spring 2019\Web and ASP.Net\web-and-asp.net\Razor\Razor\Views\Home\Index.cshtml"
                break;
        }

#line default
#line hidden
            BeginContext(711, 30, true);
            WriteLiteral("\r\n\r\n\r\n    </p>\r\n</div>\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Product> Html { get; private set; }
    }
}
#pragma warning restore 1591
