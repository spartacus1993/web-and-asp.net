﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Words.Models
{
    public class Person
    {
        public int PersonId { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter a last name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [DisplayFormat(DataFormatString = "{MM/dd/yyyy}")]
        [Display(Name="Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [DisplayFormat(DataFormatString = "{MM/dd/yyyy}")]
        [Display(Name="Date Of Death")]
        public DateTime? DateOfDeath { get; set; }

        public byte[] Picture { get; set; }
        public string PictureContentType { get; set; }

        /*Associations*/
        public List<Quote> Quotes { get; set; }

        /*Methods*/
        public string GetFormattedNameLastFirst()
        {
            if (LastName != null && FirstName != null && MiddleName != null)
            {
                return $"{LastName}, {FirstName} {MiddleName}";
            }
            else if (LastName != null && FirstName != null)
            {
                return $"{LastName}, {FirstName}";
            }
            else if (LastName != null)
            {
                return $"{LastName}";
            }
            else if (FirstName != null)
            {
                return $"{FirstName}";
            }
            else
            {
                return "Unknown";
            }            
        }

        public string GetFormattedName()
        {
            if (LastName != null && FirstName != null && MiddleName != null)
            {
                return $"{FirstName} {MiddleName} {LastName}";
            }
            else if (LastName != null && FirstName != null)
            {
                return $"{FirstName} {LastName}";
            }
            else if (LastName != null)
            {
                return $"{LastName}";
            }
            else if (FirstName != null)
            {
                return $"{FirstName}";
            }
            else
            {
                return "Unknown";
            }
        }
    }
}
