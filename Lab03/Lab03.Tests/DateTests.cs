using System;
using Xunit;

namespace Lab03.Tests
{
    public class DateTests
    {

        public static String GetAnnouncement(DateTime datetime)
        {

            if (datetime.DayOfWeek.ToString() == "Sunday")
            {
                return "Enjoy the weekend while you can!";
            }
            if (datetime.DayOfWeek.ToString() == "Monday")
            {
                return "Looks like someone has a case of the Mondays!";
            }
            if (datetime.DayOfWeek.ToString() == "Tuesday")
            {
                return "Get some Tacos after you visit!";
            }
            if (datetime.DayOfWeek.ToString() == "Wednesday")
            {
                return "Hump Day!";
            }
            if (datetime.DayOfWeek.ToString() == "Thursday")
            {
                return "Friday's less popular brother! Weekend's almost here!";
            }
            if (datetime.DayOfWeek.ToString() == "Friday")
            {
                return "Get to hiking!";
            }
            if (datetime.DayOfWeek.ToString() == "Saturday")
            {
                return "Don't forget your fishing rod!";
            }

            return "Another beautiful day!";
        
        }

        [Fact]
        public void GetCorrectDay()
        {
            // Arrange

            string checkDay = GetAnnouncement(new DateTime(2019, 2, 22));

            // Act
            var checkAnnouncement = "Get to hiking!";

            // Assert
            Assert.Equal(checkAnnouncement, checkDay);
        }

    }
}
