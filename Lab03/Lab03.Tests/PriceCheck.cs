﻿using System;
using System.Collections.Generic;
using System.Text;
using Lab03.Controllers;
using Lab03.Models;
using Xunit;

namespace Lab03.Tests
{
    public class PriceCheck
    {
        [Fact]
        public void CorrectPrice()
        {

            // Arrange
            var testPark = new Statepark { ParkName = "FreePark", ParkLocation = "Freetown", ParkInfo = "It's free", ParkPrice = 0M };

            // Act
            testPark.ParkPrice = 10M;

            // Assert
            Assert.True(testPark.ParkPrice > 0);

        }

        [Fact]
        public void BlankDescription()
        {

            // Arrange
            var testPark = new Statepark { ParkName = "FreeTown", ParkLocation = "Freetown", ParkInfo = "It's free", ParkPrice = 0M };

            // Act
            testPark.ParkName = "";

            // Assert
            Assert.True(testPark.ParkName == "");

        }

    }
}
