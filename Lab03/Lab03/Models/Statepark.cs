﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab03.Models
{
    public class Statepark
    {
        public string ParkLocation { get; set; }
        public string ParkName { get; set; }
        public decimal ParkPrice { get; set; }
        public string ParkInfo { get; set; }
    }
}
