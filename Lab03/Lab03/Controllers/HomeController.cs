﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab03.Models;

namespace Lab03.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            Statepark[] array =
            {
                new Statepark {
                    ParkName = "Minneopa State Park",
                    ParkLocation = "54497 Gadwall Rd, Mankato, MN 56001",
                    ParkPrice = 5M,
                    ParkInfo = "It was established in 1905 to preserve Minneopa Falls, a large waterfall for southern Minnesota."
                },

                new Statepark {
                    ParkName = "Nerstrand State Park",
                    ParkLocation = "9700 170th St E, Nerstrand, MN 55053",
                    ParkPrice = 7M,
                    ParkInfo = "The park derives its name from the Big Woods, a large, contiguous forested area covering much of central Minnesota prior to the arrival of European settlers."
                },

                new Statepark {
                    ParkName = "Afton State Park",
                    ParkLocation = "6959 Peller Ave S, Hastings, MN 55033",
                    ParkPrice = 12M,
                    ParkInfo = "Its hiking trails offer views of the river, rolling glacial moraine, and bluffland it preserves."
                }

            };

            ViewBag.Weather = "Cloudy";

            ViewBag.Date = DateTime.Now.ToString("MM/dd/yyyy");

            ViewBag.Day = DateTime.Now.DayOfWeek;

            ViewBag.Announcement = DateTime.Now.DayOfWeek.ToString();

            return View(array);
                
        }
    }
}