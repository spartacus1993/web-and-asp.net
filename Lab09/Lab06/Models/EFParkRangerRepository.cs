﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFParkRangerRepository : IParkRangerRepository
    {
        private StateParkDbContext context;
        public IEnumerable<ParkRanger> ParkRangers => context.ParkRangers;

        public EFParkRangerRepository(StateParkDbContext context)
        {
            this.context = context;
        }
    }
}
