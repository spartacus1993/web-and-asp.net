﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFOrderRepository: IOrderRepository
    {
        private StateParkDbContext context;

        public EFOrderRepository(StateParkDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Order> Orders => context.Orders
            .Include(o => o.Lines)
            .ThenInclude(l => l.ParkRanger);

        public void SaveOrder(Order order)
        {
            context.AttachRange(order.Lines.Select(l => l.ParkRanger));
            if(order.OrderId == 0)
            {
                context.Orders.Add(order);
            }

        }


    }
}
