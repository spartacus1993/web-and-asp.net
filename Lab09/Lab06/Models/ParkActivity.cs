﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class ParkActivity
    {
        public int ParkActivityId { get; set; }
        public string Name { get; set; }
        public bool? MaterialsProvided { get; set; }
        public bool? Seasonal { get; set; }

         // Associations
         public List<StateParkActivity> StateParkActivities { get; set; }


    }
}
