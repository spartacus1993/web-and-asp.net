﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lab06.Migrations
{
    public partial class Migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ParkActivities",
                columns: table => new
                {
                    ParkActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    MaterialsProvided = table.Column<bool>(nullable: true),
                    Seasonal = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkActivities", x => x.ParkActivityId);
                });

            migrationBuilder.CreateTable(
                name: "ParkRangers",
                columns: table => new
                {
                    ParkRangerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    BachelorDegree = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkRangers", x => x.ParkRangerId);
                });

            migrationBuilder.CreateTable(
                name: "StateParks",
                columns: table => new
                {
                    StateParkId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    ParkRangerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateParks", x => x.StateParkId);
                    table.ForeignKey(
                        name: "FK_StateParks_ParkRangers_ParkRangerId",
                        column: x => x.ParkRangerId,
                        principalTable: "ParkRangers",
                        principalColumn: "ParkRangerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StateParkActivities",
                columns: table => new
                {
                    StateParkActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StateParkId1 = table.Column<string>(nullable: true),
                    StateParkId = table.Column<int>(nullable: false),
                    ParkActivityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateParkActivities", x => x.StateParkActivityId);
                    table.ForeignKey(
                        name: "FK_StateParkActivities_ParkActivities_ParkActivityId",
                        column: x => x.ParkActivityId,
                        principalTable: "ParkActivities",
                        principalColumn: "ParkActivityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StateParkActivities_StateParks_StateParkId1",
                        column: x => x.StateParkId1,
                        principalTable: "StateParks",
                        principalColumn: "StateParkId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StateParkActivities_ParkActivityId",
                table: "StateParkActivities",
                column: "ParkActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_StateParkActivities_StateParkId1",
                table: "StateParkActivities",
                column: "StateParkId1");

            migrationBuilder.CreateIndex(
                name: "IX_StateParks_ParkRangerId",
                table: "StateParks",
                column: "ParkRangerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StateParkActivities");

            migrationBuilder.DropTable(
                name: "ParkActivities");

            migrationBuilder.DropTable(
                name: "StateParks");

            migrationBuilder.DropTable(
                name: "ParkRangers");
        }
    }
}
