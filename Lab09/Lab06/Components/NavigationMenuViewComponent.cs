﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;

namespace Lab06.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IParkRangerRepository repository;

        public NavigationMenuViewComponent(IParkRangerRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["state"];
            return View(repository.ParkRangers
                .Select(x => x.State)
                .Distinct()
                .OrderBy(x => x));
        }

    }
}
