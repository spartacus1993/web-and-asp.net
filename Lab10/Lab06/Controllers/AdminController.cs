﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class AdminController : Controller
    {
        private IParkRangerRepository parkRangerRepo;

        public AdminController(IParkRangerRepository parkRangerRepo)
        {
            this.parkRangerRepo = parkRangerRepo;
        }

        public IActionResult Index() => View(parkRangerRepo.ParkRangers);

        public ViewResult Edit(int parkRangerId) =>
            View(parkRangerRepo.ParkRangers
                .FirstOrDefault(pr => pr.ParkRangerId == parkRangerId));

        [HttpPost]
        public IActionResult Edit(ParkRanger parkRanger)
        {
            if (!ModelState.IsValid)
            {
                return View(parkRanger);
            }
            else
            {
                parkRangerRepo.Save(parkRanger);
                TempData["message"] = $"{parkRanger.FirstName} {parkRanger.LastName} has been saved";
                return RedirectToAction("Index");
            }
        }

        public IActionResult Create()
        {
            return View("Edit", new ParkRanger());
        }

        public IActionResult Delete(int parkRangerId)
        {
            ParkRanger pr = parkRangerRepo.Delete(parkRangerId);
            TempData["message"] = $"{pr.FirstName} {pr.LastName} has been deleted";
            return RedirectToAction("Index");
        }





    }
}