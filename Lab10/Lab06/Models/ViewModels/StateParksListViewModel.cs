﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models.ViewModels
{
    public class StateParksListViewModel
    {
        public IEnumerable<StatePark> StateParks { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
